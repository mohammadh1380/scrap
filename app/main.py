from fastapi import FastAPI

from app.routers import hashtag, tweet

app = FastAPI()

#
# app.include_router(auth.router, tags=['Auth'], prefix='/api/auth')
# app.include_router(user.router, tags=['Users'], prefix='/api/users')
app.include_router(hashtag.router, tags=['Hashtags'], prefix='/api/hashtags')
app.include_router(tweet.router, tags=['Tweets'], prefix='/api/tweets')


@app.get("/")
def root():
    return {"message": "Welcome to Scrap with MongoDB and fastapi"}
