from datetime import datetime
from typing import List
from pydantic import BaseModel, EmailStr, constr
from bson.objectid import ObjectId


class HashtagBaseSchema(BaseModel):
    name: str
    created_at: datetime | None = None
    updated_at: datetime | None = None

    class Config:
        orm_mode = True


class CreateHashtagSchema(HashtagBaseSchema):
    pass


class TweetBaseSchema(BaseModel):
    _id: str
    hashtag: str
    data: dict
    created_at: datetime | None = None

    class Config:
        orm_mode = True


class CreateTweetSchema(TweetBaseSchema):
    pass

