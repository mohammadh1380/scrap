def hashtagEntity(hashtag) -> dict:
    return {
        "id": str(hashtag["_id"]),
        "name": hashtag["name"],
        "created_at": hashtag["created_at"],
        "updated_at": hashtag["updated_at"]
    }


def hashtagResponseEntity(hashtag) -> dict:
    return {
        "id": str(hashtag["_id"]),
        "name": hashtag["name"],
        "created_at": hashtag["created_at"],
        "updated_at": hashtag["updated_at"]
    }


def embeddedHashtagResponse(hashtag) -> dict:
    return {
        "id": str(hashtag["_id"]),
        "name": hashtag["name"],
    }


def hashtagListEntity(hashtags) -> list:
    return [hashtagEntity(hashtag) for hashtag in hashtags]
