def tweetEntity(tweet) -> dict:
    return {
        "id": str(tweet["_id"]),
        "data": tweet["data"],
        "created_at": tweet["created_at"]
    }


def tweetResponseEntity(tweet) -> dict:
    return {
        "id": str(tweet["_id"]),
        "data": tweet["data"],
        "created_at": tweet["created_at"]
    }


def embeddedTweetResponse(tweet) -> dict:
    return {
        "id": str(tweet["_id"]),
        "data": tweet["data"],
    }


def tweetListEntity(tweets) -> list:
    return [tweetEntity(tweet) for tweet in tweets]
