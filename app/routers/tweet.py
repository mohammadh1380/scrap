from datetime import datetime
import re
import requests
from fastapi import HTTPException, status, APIRouter
from app.database import Tweet, Hashtag
from app.serializers.hashtag_serializers import hashtagListEntity
from app.serializers.tweet_serializers import tweetListEntity, tweetEntity
from pymongo.errors import DuplicateKeyError, InvalidName, ExecutionTimeout
from ..utils.interval import perpetualTimer

# [...] imports

router = APIRouter()


@router.get('/getAutoTweetByHashtag', status_code=status.HTTP_200_OK)
def get_tweet():
    pipeline = [
        {'$match': {}}
    ]
    try:
        hashtags = hashtagListEntity(list(Hashtag.aggregate(pipeline)))

        ids = ""
        for i in hashtags:
            for j in range(0, 11):
                payload = {"page": j, "tag": i.get("name"), "id": ids}
                headers = {
                    'User-Agent': 'My User Agent 1.0',
                }
                r = requests.post('https://www.sahamyab.com/guest/twiter/list?v=0.1', headers=headers,
                                  data=payload).json()

                for k in r.get("items"):
                    ids = k.get("id")
                    try:
                        tweet = {"_id": k.get("id"), "data": k, "hashtag": i.get("name"),
                                 "created_at": datetime.utcnow()}
                        result = Tweet.insert_one(tweet)
                    except DuplicateKeyError:
                        print("Tweet exist")
    except InvalidName:
        print("error")

    return {"msg": "Tweets created in database"}


def crawl():
    data = {"page": 0}
    headers = {
        'User-Agent': 'My User Agent 1.0',
    }
    r = requests.post('https://www.sahamyab.com/guest/twiter/list?v=0.1', headers=headers, data=data).json()

    for i in r.get("items"):
        if i.get("content"):
            e = re.split(' |\n', i.get("content"))

            for j in e:
                try:
                    if j != '':
                        if j[0] == '#':
                            hashtag = j.split("#")[1]
                            pipeline = [
                                {'$match': {"name": hashtag}}
                            ]
                            res = list(Hashtag.aggregate(pipeline))
                            if len(res) > 0:
                                try:
                                    tweet = {"_id": i.get("id"), "data": i, "hashtag": hashtag,
                                             "created_at": datetime.utcnow()}
                                    result = Tweet.insert_one(tweet)
                                except DuplicateKeyError:
                                    print("Tweet exist")
                except ValueError:
                    print(j[0])


timer = perpetualTimer(10, crawl)


@router.get('/startCrawl', status_code=status.HTTP_200_OK)
def start_crawl():
    timer.start()
    return {"msg": "Interval Start"}


@router.get('/stopCrawl', status_code=status.HTTP_200_OK)
def stop_crawl():
    timer.cancel()
    return {"msg": "Interval Start"}


@router.get('/getTweets', status_code=status.HTTP_200_OK)
def tweets_list():
    pipeline = [
        {'$match': {}}
    ]
    try:
        result = tweetListEntity(list(Tweet.aggregate(pipeline)))
    except ExecutionTimeout:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=f"failed")
    return result


@router.post('/getTweetsByHashtag', status_code=status.HTTP_201_CREATED)
def tweets_list(name: str):
    print(name)
    pipeline = [
        {'$match': {"hashtag": name}}
    ]
    try:
        result = tweetListEntity(list(Tweet.aggregate(pipeline)))
    except ExecutionTimeout:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=f"failed")
    return result
