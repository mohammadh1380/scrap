from datetime import datetime

import requests
from fastapi import Depends, HTTPException, status, APIRouter, Response
from pymongo.collection import ReturnDocument
from app import schemas
from app.database import Hashtag, Tweet
from app.serializers.hashtag_serializers import hashtagEntity, hashtagListEntity
from bson.objectid import ObjectId
from pymongo.errors import DuplicateKeyError

# [...] imports

router = APIRouter()


@router.get('/getAutoHashtag', status_code=status.HTTP_200_OK)
def get_hashtag():
    r = requests.get('https://data.nadpco.com/v1/BaseInfo/companies')

    for i in r.json():
        try:
            if i.get("CoTSESymbol"):
                hashtag = {"name": i.get("CoTSESymbol"), "created_at": datetime.utcnow(),
                           "updated_at": datetime.utcnow()}
                result = Hashtag.insert_one(hashtag)
        except DuplicateKeyError:
            print("hashtag exist")

    return {"msg": "hashtags created in database"}


@router.post('/', status_code=status.HTTP_201_CREATED)
def create_hashtag(hashtag: schemas.CreateHashtagSchema):
    hashtag.created_at = datetime.utcnow()
    hashtag.updated_at = hashtag.created_at
    try:
        result = Hashtag.insert_one(hashtag.dict())
        if result:
            ids = ""
            print(hashtag.name)
            for j in range(0, 11):
                payload = {"page": j, "tag": hashtag.name, "id": ids}
                headers = {
                    'User-Agent': 'My User Agent 1.0',
                }
                r = requests.post('https://www.sahamyab.com/guest/twiter/list?v=0.1', headers=headers,
                                  data=payload).json()

                for k in r.get("items"):
                    ids = k.get("id")
                    try:
                        tweet = {"_id": k.get("id"), "data": k, "hashtag": hashtag.name,
                                 "created_at": datetime.utcnow()}
                        result1 = Tweet.insert_one(tweet)
                    except DuplicateKeyError:
                        print("Tweet exist")
        pipeline = [
            {'$match': {'_id': result.inserted_id}}
        ]
        new_post = hashtagListEntity(list(Hashtag.aggregate(pipeline)))[0]
        return new_post
    except DuplicateKeyError:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT,
                            detail=f"Hashtag with name: '{hashtag.name}' already exists")


