from pymongo import mongo_client
import pymongo
from app.config import settings

client = mongo_client.MongoClient(settings.DATABASE_URL)
print('Connected to MongoDB...')

db = client[settings.MONGO_INITDB_DATABASE]
Hashtag = db.hashtag
Hashtag.create_index([("name", pymongo.ASCENDING)], unique=True)
Tweet = db.tweet
Tweet.create_index([("_id", pymongo.ASCENDING)])
